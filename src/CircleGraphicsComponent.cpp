// include header files
#include "CircleGraphicsComponent.hpp"

CircleGraphicsComponent::CircleGraphicsComponent()
{
	mCircleShape = new sf::CircleShape( 100.f );
	mCircleShape->setFillColor( sf::Color::Blue );
}

CircleGraphicsComponent::~CircleGraphicsComponent()
{
	delete mCircleShape;
}

void CircleGraphicsComponent::update( GameObject& gameObject )
{
	mCircleShape->setPosition( gameObject.posX, gameObject.posY );
}

void CircleGraphicsComponent::interpolate( GameObject& gameObject )
{
	mCircleShape->setPosition( gameObject.posX, gameObject.posY );
}

void CircleGraphicsComponent::draw( sf::RenderWindow& renderWindow )
{
	renderWindow.draw( *mCircleShape );
}
