// including headers
#include <iostream>
#include "Application.hpp"

int main(int, char**)
{
	Application app;
	
	if( !app.init() )
	{
		std::cout << "Application was not able to initialize!" << std::endl;
	}
	else
	{
		app.run();
	}
	
	std::cout << "Application is shutting down..." << std::endl;

	app.terminate();

	return 0;
}
