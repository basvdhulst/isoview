// include project header files
#include "GameStateMainMenu.hpp"
#include "Application.hpp"

// include SFML header files
#include <SFML/Window/Event.hpp>
#include <SFML/Window/Keyboard.hpp>
#include <SFML/Graphics/Color.hpp>

// REMOVE!
#include <iostream>

GameStateMainMenu::GameStateMainMenu( Application& application ) :
	GameState( application )
{
}

void GameStateMainMenu::enter()
{
}

void GameStateMainMenu::exit()
{
	mApplication->popGameState();
}

void GameStateMainMenu::draw( sf::RenderWindow& renderWindow )
{
	renderWindow.clear( sf::Color::Green );
}

void GameStateMainMenu::handleInput( sf::RenderWindow& renderWindow )
{
	sf::Event event;

	while( renderWindow.pollEvent( event ) )
	{
		if( event.type == sf::Event::KeyPressed )
		{
			switch( event.key.code )
			{
				case sf::Keyboard::P:
					mApplication->pushGameState( Application::GameStateEnum::WORLD );
					break;
				case sf::Keyboard::Escape:
					exit();
					break;
			}
		}
	}
}

void GameStateMainMenu::interpolate( const int dt, const float timeFraction )
{
}

void GameStateMainMenu::update( const int dt )
{
}
