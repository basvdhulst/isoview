// include project header files
#include "GameStateWorld.hpp"
#include "Application.hpp"
#include "CircleGraphicsComponent.hpp"

// include SFML header files
#include <SFML/Window/Event.hpp>
#include <SFML/Window/Keyboard.hpp>

GameStateWorld::GameStateWorld( Application& application ) :
	GameState( application ),
	mCircle( new CircleGraphicsComponent() )
{
	enter();
}

void GameStateWorld::enter()
{
	mWorld.generateTerrain();
}

void GameStateWorld::exit()
{
	mApplication->popGameState();
}

void GameStateWorld::draw( sf::RenderWindow& renderWindow )
{
	// draw game objects
	mWorld.draw( renderWindow );
	mCircle.draw( renderWindow );
}

void GameStateWorld::handleInput( sf::RenderWindow& renderWindow )
{
	sf::Event event;

	while( renderWindow.pollEvent( event ) )
	{
		switch( event.key.code )
		{
			case sf::Keyboard::Escape:
				exit();
				break;
		}
	}
}

void GameStateWorld::interpolate( const int dt, const float timeFraction )
{
	mCircle.interpolate( dt, timeFraction );
}

void GameStateWorld::update( const int dt )
{
	mCircle.update( dt );
}
