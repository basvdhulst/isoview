// include header files
#include "Application.hpp"
//#include "GameState.hpp"

Application::Application() :
	mRenderWindow( sf::VideoMode( SCREEN_WIDTH, SCREEN_HEIGHT), "MyWindow" ),
	mGameStateMainMenu( *this ),
	mGameStateWorld( *this )
{
}

Application::~Application()
{
}

bool Application::init()
{
	// initialize service providers
	//Locator::provide( &mInputHandler );	

	// push main menu game state
	pushGameState( GameStateEnum::MAIN_MENU );

	return true;
}

void Application::terminate()
{
}

void Application::run()
{
	int timePrevious = mClock.getElapsedTime().asMilliseconds();
	int timeElapsed = 0;

	while( mRenderWindow.isOpen() && !mGameStateStack.empty() )
	{
		int framesSkipped = 0;
		int timeCurrent = mClock.getElapsedTime().asMilliseconds();
		timeElapsed += timeCurrent - timePrevious;
		timePrevious = mClock.getElapsedTime().asMilliseconds();

		// handle input
		//Locator::getInputHandler().handleInput( mRenderWindow );
		if( !mGameStateStack.empty() )
		{
			mGameStateStack.top()->handleInput( mRenderWindow );
		}

		while( timeElapsed >= MS_PER_UPDATE && framesSkipped < MAX_FRAME_SKIP )
		{
			// update
			if( !mGameStateStack.empty() )
			{
				mGameStateStack.top()->update( MS_PER_UPDATE );
			}
			timeElapsed -= MS_PER_UPDATE;
			framesSkipped++;
		}

		// interpolate
		const float timeFraction = (float)timeElapsed/MS_PER_UPDATE;
		if( !mGameStateStack.empty() )
		{
			mGameStateStack.top()->interpolate( MS_PER_UPDATE, timeFraction );
		}

		// draw to screen
		mRenderWindow.clear( sf::Color::Black );
		if( !mGameStateStack.empty() )
		{
			mGameStateStack.top()->draw( mRenderWindow );
		}
		mRenderWindow.display();
	}
}

void Application::changeGameState( GameStateEnum gameStateEnum )
{
	popGameState();
	pushGameState( gameStateEnum );
}

void Application::popGameState()
{
	if( !mGameStateStack.empty() )
	{
		mGameStateStack.pop();
	}
}

void Application::pushGameState( GameStateEnum gameStateEnum )
{
	switch( gameStateEnum )
	{
		case GameStateEnum::INTRO:
			break;
		case GameStateEnum::MAIN_MENU:
			mGameStateStack.push( &mGameStateMainMenu );
			break;
		case GameStateEnum::WORLD:
			mGameStateStack.push( &mGameStateWorld );
			break;
	}

	}
