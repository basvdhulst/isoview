// include header files
#include "Terrain.hpp"

Terrain::Terrain( TerrainType terrainType ) :
	mTerrainType( terrainType )
{
}

void Terrain::init()
{
	switch( mTerrainType )
	{
		case TERRAIN_GRASS:
			mTexture.loadFromFile("tile_grass.png");
			break;
		case TERRAIN_WATER:
			mTexture.loadFromFile("building1.png");
			break;
	}
}

const sf::Texture& Terrain::getTexture() const
{
	return mTexture;
}
