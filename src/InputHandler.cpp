// include project header files
#include "InputHandler.hpp"

// include SFML header files
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Window/Event.hpp>

void InputHandler::handleInput( sf::RenderWindow& renderWindow )
{
	sf::Event event;

	while( renderWindow.pollEvent( event ) )
	{
		// handle window events
		if( event.type == sf::Event::Closed )
		{
			renderWindow.close();
		}

	}

}
