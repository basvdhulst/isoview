// include header files
#include "GameObject.hpp"
#include "GraphicsComponent.hpp"

GameObject::GameObject( GraphicsComponent* graphicsComponent ) :
	posX(0.f), posY(50.f), prevPosX(0.f), prevPosY(0.f), velX(0.4f), velY(0.f),
	mGraphicsComponent(graphicsComponent)
{
}

GameObject::~GameObject()
{
	if( mGraphicsComponent ){ delete mGraphicsComponent; }
}

void GameObject::update( const int dt )
{
	posX = prevPosX + velX * dt;
	posY = prevPosY + velY * dt;
	
	if( posX > 1440 ){ posX -= 1440; }
	if( posY > 990 ){ posY -= 990; }

	prevPosX = posX;
	prevPosY = posY;
	
	mGraphicsComponent->update( *this );
}

void GameObject::interpolate( const int dt, const float timeFraction )
{
	posX = prevPosX + velX * dt * timeFraction;
	posY = prevPosY + velY * dt * timeFraction;

	mGraphicsComponent->interpolate( *this );
}

void GameObject::draw( sf::RenderWindow& renderWindow )
{
	mGraphicsComponent->draw( renderWindow );
}
