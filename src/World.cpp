// include header files
#include "World.hpp"

World::World() :
	mGrassTerrain( TERRAIN_GRASS ),
	mWaterTerrain( TERRAIN_WATER )
{
}

void World::draw( sf::RenderWindow& renderWindow )
{
	for( int i = 0; i < HEIGHT; ++i )
	{
		for( int j = 0; j < WIDTH; ++j )
		{
			renderWindow.draw( mTiles[i][j].getSprite() );
		}
	}
}

void World::generateTerrain()
{
	// initialize terrains
	mGrassTerrain.init();
	mWaterTerrain.init();

	// generate terrain map
	for( int i = 0; i < HEIGHT; ++i )
	{
		for( int j = 0; j < WIDTH; ++j )
		{
			sf::Vector2f pos = cartToIso( TILE_SIZE*j+500, TILE_SIZE*i );

			if( i == 1 )
			{
				mTiles[i][j].init( mGrassTerrain, pos.x, pos.y );
			}
			else
			{
				mTiles[i][j].init( mWaterTerrain, pos.x, pos.y );
			}
		}
	}	
}

sf::Vector2f World::cartToIso( float x, float y )
{
	float newx = x - y;
	float newy = (x + y) / 2;
	return sf::Vector2f( newx, newy );
}

sf::Vector2f World::isoToCart( float x, float y )
{
	int newx = (2*y + x) / 2;
	int newy = (2*y - x) / 2;
	return sf::Vector2f( newx, newy );
}
