// include header files
#include "Locator.hpp"

// static initializations
InputHandler* Locator::mInputHandler = 0;

InputHandler& Locator::getInputHandler()
{
	return *mInputHandler;
}

void Locator::init()
{
}

void Locator::provide( InputHandler* inputHandler )
{
	mInputHandler = inputHandler;
}
