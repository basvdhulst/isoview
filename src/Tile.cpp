// include header files
#include "Tile.hpp"

Tile::Tile() :
	mTerrain(NULL)
{
}

void Tile::init( Terrain& terrain, float x, float y )
{
	mTerrain = &terrain;
	mSprite.setTexture( mTerrain->getTexture() );
	mSprite.setOrigin( 100.f, mTerrain->getTexture().getSize().y - 50.f );
	mSprite.setPosition( x, y );
}

const sf::Sprite& Tile::getSprite() const
{
	return mSprite;
}
