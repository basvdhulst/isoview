#ifndef GAMESTATEMAINMENU_HPP
#define GAMESTATEMAINMENU_HPP

// include SFML header files
#include <SFML/Graphics/View.hpp>

// include project header files
#include "GameState.hpp"

class GameStateMainMenu : public GameState
{
public:
	GameStateMainMenu( Application& application );

	virtual void enter();
	virtual void exit();
	virtual void draw( sf::RenderWindow& renderWindow );
	virtual void handleInput( sf::RenderWindow& renderWindow );
	virtual void interpolate( const int dt, const float timeFraction );
	virtual void update( const int dt );

private:
	sf::View mView;
};

#endif // GAMESTATEMAINMENU_HPP
