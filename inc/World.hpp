#ifndef WORLD_HPP
#define WORLD_HPP

// include SFML header files
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/System/Vector2.hpp>

// include project header files
#include "Terrain.hpp"
#include "Tile.hpp"

class World
{
public:
	World();

	void draw( sf::RenderWindow& renderWindow );
	void generateTerrain();

private:
	static constexpr float TILE_SIZE = 100.f;
	static const int WIDTH = 7;
	static const int HEIGHT = 5;
	Terrain mGrassTerrain;
	Terrain mWaterTerrain;
	Tile mTiles[HEIGHT][WIDTH];

	sf::Vector2f cartToIso( float x, float y );
	sf::Vector2f isoToCart( float x, float y );
};

#endif // WORLD_HPP
