#ifndef GAMESTATEWORLD_HPP
#define GAMESTATEWORLD_HPP

// include SFML header files
#include <SFML/Graphics/View.hpp>

// include project header files
#include "GameState.hpp"
#include "World.hpp"
#include "GameObject.hpp"

// forward class declarations
class Application;

class GameStateWorld : public GameState
{
public:
	GameStateWorld( Application& application );

	virtual void enter();
	virtual void exit();
	virtual void draw( sf::RenderWindow& renderWindow );
	virtual void handleInput( sf::RenderWindow& renderWindow );
	virtual void interpolate( const int dt, const float timeFraction );
	virtual void update( const int dt );

private:
	sf::View mWorldView;
	sf::View mGuiView;
	World mWorld;
	GameObject mCircle;
};

#endif // GAMESTATEWORLD_HPP
