#ifndef LOCATOR_HPP
#define LOCATOR_HPP

// include project header files
#include "InputHandler.hpp"

class Locator
{
public:
	static InputHandler& getInputHandler();
	static void init();
	static void provide( InputHandler* inputHandler );

private:
	static InputHandler* mInputHandler;
};

#endif // LOCATOR_HPP
