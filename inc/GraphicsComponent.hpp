#ifndef GRAPHICSCOMPONENT_HPP
#define GRAPHICSCOMPONENT_HPP

// include SFML header files
#include <SFML/Config.hpp>
#include <SFML/Graphics/RenderWindow.hpp>

// include project header files
#include "GameObject.hpp"

class GraphicsComponent
{
public:
	virtual ~GraphicsComponent() {}
	virtual void update( GameObject& gameObject ) = 0;
	virtual void interpolate( GameObject& gameObject ) = 0;
	virtual void draw( sf::RenderWindow& renderWindow ) = 0;
};

#endif // GRAPHICSCOMPONENT_HPP
