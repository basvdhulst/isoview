#ifndef INPUTHANDLER_HPP
#define INPUTHANDLER_HPP

// include SFML header files
#include <SFML/Graphics/RenderWindow.hpp>

class InputHandler
{
public:
	void handleInput( sf::RenderWindow& renderWindow );
};

#endif // INPUTHANDLER_HPP
