#ifndef CIRCLEGRAPHICSCOMPONENT_HPP
#define CIRCLEGRAPHICSCOMPONENT_HPP

// include SFML header files
#include <SFML/Config.hpp>
#include <SFML/Graphics/CircleShape.hpp>
#include <SFML/Graphics/Color.hpp>

// include project header files
#include "GraphicsComponent.hpp"

class CircleGraphicsComponent : public GraphicsComponent
{
public:
	CircleGraphicsComponent();
	~CircleGraphicsComponent();
	virtual void update( GameObject& gameObject );
	virtual void interpolate( GameObject& gameObject );
	virtual void draw( sf::RenderWindow& renderWindow );

private:
	sf::CircleShape* mCircleShape;
};

#endif // CIRCLEGRAPHICSCOMPONENT_HPP
