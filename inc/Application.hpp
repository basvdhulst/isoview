#ifndef APPLICATION_H
#define APPLICATION_H

// include standard header files
#include <iostream>
#include <stack>

// include SFML header files
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/System/Clock.hpp>

// include project header files
//#include "Locator.hpp"
//#include "InputHandler.hpp"
#include "GameStateMainMenu.hpp"
#include "GameStateWorld.hpp"

// forward class declarations
class GameState;

class Application
{
public:

	enum class GameStateEnum
	{
		INTRO = 0,
		MAIN_MENU,
		WORLD
	};

	Application();
	~Application();

	bool init();
	void run();
	void terminate();

	void changeGameState( GameStateEnum gameStateEnum );
	void popGameState();
	void pushGameState( GameStateEnum gameStateEnum );

private:
	static const int MS_PER_UPDATE = 1000 / 100;
	static const int MAX_FRAME_SKIP = 5;
	static const int SCREEN_WIDTH = 1440;
	static const int SCREEN_HEIGHT = 900;

	sf::RenderWindow mRenderWindow;
	//InputHandler mInputHandler;
	sf::Clock mClock;
	
	std::stack<GameState*> mGameStateStack;
	GameStateMainMenu mGameStateMainMenu;
	GameStateWorld mGameStateWorld;
};

#endif // APPLICATION_H
