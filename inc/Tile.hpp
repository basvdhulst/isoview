#ifndef TILE_HPP
#define TILE_HPP

// include SFML header files
#include <SFML/Graphics/Sprite.hpp>

// include project header files
#include "Terrain.hpp"

class Tile
{
public:
	Terrain* mTerrain;
	
	Tile();
	void init( Terrain& terrain, float x, float y );
	const sf::Sprite& getSprite() const;

private:
	sf::Sprite mSprite;
};

#endif // TILE_HPP
