#ifndef GAMEOBJECT_HPP
#define GAMEOBJECT_HPP

// include SFML header files
#include <SFML/Graphics/RenderWindow.hpp>

// forward class declarations
class GraphicsComponent;

class GameObject
{
public:
	GameObject( GraphicsComponent* graphicsComponent );
	~GameObject();

	void update( const int dt );
	void interpolate( const int dt, const float timeFraction );
	void draw( sf::RenderWindow& renderWindow );

	float posX, posY, prevPosX, prevPosY, velX, velY;

private:
	GraphicsComponent* mGraphicsComponent;
};

#endif // GAMEOBJECT_HPP
