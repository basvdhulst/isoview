#ifndef TERRAINTYPE_HPP
#define TERRAINTYPE_HPP

// include SFML header files
#include <SFML/Graphics/Texture.hpp>

enum TerrainType
{
	TERRAIN_GRASS,
	TERRAIN_WATER
};

class Terrain
{
public:
	Terrain( TerrainType terrainType );

	void init();
	const sf::Texture& getTexture() const;

private:
	TerrainType mTerrainType;
	sf::Texture mTexture;
};

#endif // TERRAINTYPE_HPP
