#ifndef INPUTCOMPONENT_HPP
#define INPUTCOMPONENT_HPP

class InputComponent
{
public:
	virtual ~InputComponent() {}
	virtual void update() = 0;
};

#endif // INPUTCOMPONENT_HPP
