#ifndef GAMESTATE_HPP
#define GAMESTATE_HPP

// include SFML header files
#include <SFML/Graphics/RenderWindow.hpp>

// forward class declaration
class Application;

class GameState
{
public:
	virtual void enter() = 0;
	virtual void exit() = 0;
	virtual void draw( sf::RenderWindow& renderWindow ) = 0;
	virtual void handleInput( sf::RenderWindow& renderWindow ) = 0;
	virtual void interpolate( const int dt, const float timeFraction ) = 0;
	virtual void update( const int dt ) = 0;

protected:
	GameState( Application& application );

	Application* mApplication;
};

#endif // GAMESTATE_HPP
