************************
Isoview (2015)
************************

Rendering of a 2D-isometric world, using the SFML graphics library.
Implementation of the ideas of Robert Nystrom from his book "Game 
Programming Patterns". Classes were created to form a general 
framework that can be reused and expanded in future projects.

Push 'P' to begin, 'ESC' to quit.

Libraries: SFML
